require 'libs/class'
local anim8=require 'libs.anim8'

class"player"
function player:player(x,y)
  self.x=x
  self.y=y
  self.w=32
  self.h=32
  self.xdest=self.x
  self.ydest=self.y
  self.cooldown=0
  self.gold=0
  self.typ="player"
  self.image=love.graphics.newImage('graphics/player_walk.png')
  self.g=anim8.newGrid(64,64,self.image:getWidth(),self.image:getHeight())
  self.animation_r=anim8.newAnimation(self.g('1-9',4),0.001)
  self.animation_l=anim8.newAnimation(self.g('1-9',2),0.001)
  self.animation_u=anim8.newAnimation(self.g('1-9',1),0.001)
  self.animation_d=anim8.newAnimation(self.g('1-9',3),0.001)
  self.directions={"up","right","down","left"}
  self.stani=2
  self.stan='right'
  self.inventory={}
  self.hands=nil
  self.grab_on=false
  self.grab_object_params={}
  self.grab_object_params.color=0
  self.ready=true
end

function player:key_callback(key)
  if key=="d" then
    self.xdest=self.xdest+32  

  elseif key=="a" then
    self.xdest=self.xdest-32

  elseif key=="s" then
    self.ydest=self.ydest+32
  elseif key=="w" then
    self.ydest=self.ydest-32
  elseif key=="g"  then if self.hands then self:drop() else self.grab_on=true end
  end
end

function player:addItem(item)
  table.insert(self.inventory,item)
end

function player:animationHandle()
end

function player:update(dt)
  self.cooldown=math.max(self.cooldown-dt,0)
  self:resolveActions(dt)
end

local selfFilter = function(item, other)
  if     other.typ=="coin" or other.typ=="plate_box" or other.typ=="plate_pillar"  then return 'cross'
  else return 'slide'
  end
end

function player:resolveCollisions()
  for i,col in pairs(cols) do
    if col.other.typ=="coin" then 
      self.gold=self.gold+1 
      col.other.hidden=1 
      self:addItem(col.other) 
      col.other:remove() 
    elseif col.other.typ=="box" or col.other.typ=="pillar" then
      if not self.grab_on  or self.hands then
        if col.normal.y==1 then col.other:move(col.other.x,col.other.y-32)
        elseif  col.normal.y==-1 then col.other:move(col.other.x,col.other.y+32)
        elseif  col.normal.x==-1 then col.other:move(col.other.x+32,col.other.y)
        elseif  col.normal.x==1 then col.other:move(col.other.x-32,col.other.y)
        end
        
        --print(col)
      else

        if self.grab_object_params.color==col.other.color then
          self.hands=col.other
          self.hands:remove_col()
          self.grab_on=false
        end
      end
    elseif col.other.typ==nil then
      self.xdest=self.x self.ydest=self.y
    end
  end
end

function player:resolveAnimations(dt)
  if self.x<self.xdest then self.x=self.x+32 self.stani=2 self.animation_r:resume()
  elseif self.x>self.xdest then self.x=self.x-32 self.stani=4 self.animation_l:resume() end
  if self.y>self.ydest then self.y=self.y-32 self.stani=1 self.animation_u:resume()
  elseif self.y<self.ydest then self.y=self.y+32 self.stani=3 self.animation_d:resume() end
  self.animation_r:update(dt)
  self.animation_l:update(dt)
  self.animation_u:update(dt)
  self.animation_d:update(dt)
end

function player:resolveActions(dt)
  if self.cooldown==0 then

    self.cooldown=0.1
    self:resolveAnimations(dt)
    self.stan=self.directions[self.stani]
    self.x, self.y, cols, len = world:move( self, self.x, self.y,selfFilter )

    if self.x==self.xdest and self.y==self.ydest then self.ready=true else self.ready=false end
    if self.ready and co and coroutine.status(co)=="suspended" and action.interrupt==false then coroutine.resume(co) end

    self:setItemPos()
    self:resolveCollisions()
  end
end

function player:draw()
  love.graphics.setColor(255,255,255)

  love.graphics.rectangle( "line",self.x,self.y,self.w,self.h )
  if self.stan=='right' then self.animation_r:draw(self.image,self.x-16,self.y-32)
  elseif self.stan=='left' then self.animation_l:draw(self.image,self.x-16,self.y-32)
  elseif self.stan=='up' then self.animation_u:draw(self.image,self.x-16,self.y-32)
  elseif self.stan=='down' then self.animation_d:draw(self.image,self.x-16,self.y-32)
  else
  end
  self.animation_r:pause()
  self.animation_l:pause()
  self.animation_u:pause()
  self.animation_d:pause()
 
end

function player:drop()
  if self.hands then
    world:add(self.hands,self.hands.x,self.hands.y,self.hands.w,self.hands.h)
   self.hands:move(self.hands.x,self.hands.y)
    self.hands=nil
    self.grab_object_params={}
  end
end

function player:setItemPos()
  if self.hands then  

    if self.stan=="right" then self.hands.x=self.x+32 self.hands.y=self.y 
    elseif self.stan=="left" then self.hands.x=self.x-32 self.hands.y=self.y 
    elseif self.stan=="up" then self.hands.x=self.x self.hands.y=self.y-32 
    elseif self.stan=="down" then self.hands.x=self.x self.hands.y=self.y+32
    end

  end
end

function player:grab(object)

  self.xdest=object.x
  self.ydest=object.y
  self.grab_on=true
  self.grab_object_params.color=object.color

end
