return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.17.1",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 50,
  height = 19,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "border",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/border.bmp",
      imagewidth = 32,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "floor",
      firstgid = 2,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/floor.png",
      imagewidth = 32,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "sand",
      firstgid = 3,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/sand.jpg",
      imagewidth = 1800,
      imageheight = 1350,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 2352,
      tiles = {}
    },
    {
      name = "on_crack_by_dazzle_textures",
      firstgid = 2355,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/on_crack_by_dazzle_textures.jpg",
      imagewidth = 2592,
      imageheight = 1944,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 4860,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 1",
      x = 0,
      y = 0,
      width = 50,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["collidable"] = "true"
      },
      encoding = "base64",
      compression = "zlib",
      data = "eJztlzsKACAMxdr7X9rV9YnxAykUt4Tg1K6q/nxreqk5xbcj89B8OzIPzbcj89B8O94Y/2PNQ/PtyDw0347MQ/PtyDw0//Ytt+seHFvUAJU="
    },
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 2",
      x = 0,
      y = 0,
      width = 50,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzd0tWTV3Ucx+HjonQK7O5vd5FOpUsxUa4AEQWlS+lSupTubkQaROlSupQupUvpsmgwaB6GccZ/gHPhZ+a5fn9fc04Q/D9uUHQQDGYIQxnGcEYwklGMZgxjGcfnjOcLJjCRSUxmClOZxvToxxtRT9C/d8zWcU5wklOc5gxnOcd5LvALv/Ibv/MHF7nEZa5wlWtc50aIHfljgqAABSlEYYpQlGIUpwQleZGXKMXLvMKrvMbrvEFp3uQtysSE19HVVje604Oe9KI3fehLP/ozgIEMYjBDGMowhjOCkYxiNGNC7Nhtaw972cd+DnCQQxzmCEf5iZ85xnFOcJJTnOYMZznHeS6E2JElNgiyko3s5CAnuchNHvKSj+d5gfwUoCCFKEwRilKM4pSgZGx4Ha1staYNbWlHezrQkU50pguf8hld6UZ3etCTXvSmD33pR/8QOzbY2sgmNrOFrWxjOzvYyS5+4Ed2s4e97GM/BzjIIQ5zhKMhdqSPBEEGMhJNDLFEiCOeBDLxHJnJQlaykZ0c5CQXuclDXvJFnmzDfzvq22pAQxrRmCY0pRnNaUFLPuYTWtGaNrSlHe3pQEc60ZkukfC+x1Jby1jOClayitWsYS3rWM93fM8GNrKJzWxhK9vYzg52sivEjsRxQZCEpCQjOSlISSpSk4a0pONZ0pOBjEQTQywR4ogngUxx4XVUsVWValSnBjWpRW3qUJd6fMhH1KcBDWlEY5rQlGY0pwUtQ+yYZWs2c5jLPOazgIUsYjFL+IZvWcoylrOClaxiNWtYyzrWh9hxy9Zt7nCXe9znwaM3xAfBU0SRiKd5hsQkISnJSE4KUpKK1KQhbXx4HWVtlaM8b1OBd6jIu7xHJSrzPh9QhapUozo1qEktalOHutQLsWOCrYlMYjJTmMo0pjODL5nJV3zNLGYzh7nMYz4LWMgiFrMkxI6Lti5xmStc5RrXucFN/uQv/uYfbnGbO9zlHvd58OjdCf5FokiUEIR2DwFHQAe9"
    }
  }
}
