return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "v0.15.0-1-gb6d2a31",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 50,
  height = 19,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "border",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/border.bmp",
      imagewidth = 32,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "floor",
      firstgid = 2,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/floor.png",
      imagewidth = 32,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 1",
      x = 0,
      y = 0,
      width = 50,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["collidable"] = "true"
      },
      encoding = "base64",
      compression = "zlib",
      data = "eJztlzsKACAMxdr7X9rV9YnxAykUt4Tg1K6q/nxreqk5xbcj89B8OzIPzbcj89B8O94Y/2PNQ/PtyDw0347MQ/PtyDw0//Ytt+seHFvUAJU="
    },
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 2",
      x = 0,
      y = 0,
      width = 50,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt0cEJAAAIAsBo/6EbIuohJ/gVDqty0of92Ofg4ODgyHNc1x8cHBwcHFtHQgaQmwYf"
    }
  }
}
