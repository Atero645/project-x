return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.18.0",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 40,
  height = 19,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "terrain",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/terrain.png",
      imagewidth = 1024,
      imageheight = 1024,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1024,
      tiles = {}
    },
    {
      name = "wall",
      firstgid = 1025,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/wall.png",
      imagewidth = 32,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 2,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "ground",
      x = 0,
      y = 0,
      width = 40,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBj8YPEgxS+YRt036r6BdR8ID7Q7Rt036r7BiEfdN/zdxzAI3DHqPtpgGBhodxBy32AGAGNvsGg="
    },
    {
      type = "tilelayer",
      name = "Walls",
      x = 0,
      y = 0,
      width = 40,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["collidable"] = "true"
      },
      encoding = "base64",
      compression = "zlib",
      data = "eJxjZGFgYBzkmGmQYpj7BisYqu67wMjAcBGILzHS303IAJf7PgDd9RGIPw1S9wkwMTAIArEQE/3dhAyGavrDB7bRxilYwXAMP3qCUfdRBsgpnwdD/hgtn4kDQzX9DRYw0O1iYjAAo3MPlQ=="
    },
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 3",
      x = 0,
      y = 0,
      width = 40,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztlNsNgCAMRRtZQB3KXX3tok6iTmD5IEEDeAUVTTjJ/aP0gBWixN8YMqKRM3FaQdRxeoHX17y2EX61CDN7LZz1o34571dwypt7+HjLGlPd03eAYvOI5Yf2Veve9KzEPi6SX7if7d/xRX9Xz3q7etq8+hM/W41Cf1dD/JBepvO4IvfS39UQv7u/61WQu0Fn5S2/I+isxPJDZyURxgYUxHaS"
    }
  }
}
