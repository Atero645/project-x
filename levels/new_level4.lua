return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.18.0",
  orientation = "orthogonal",
  renderorder = "right-up",
  width = 40,
  height = 19,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "terrain",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/terrain.png",
      imagewidth = 1024,
      imageheight = 1024,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1024,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Walls",
      x = 0,
      y = 0,
      width = 40,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["collidable"] = "true"
      },
      encoding = "base64",
      compression = "zlib",
      data = "eJzt1rENAAAIwzD4/1Z+4AWkDHSIpe4ZO1014UtlH2MfYx9jH2MfYx9jH2Mf8/2LL1tMkGn9"
    },
    {
      type = "tilelayer",
      name = "ground",
      x = 0,
      y = 0,
      width = 40,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzdlEsOwCAIRO2BLfffeQGB4ddMasJO4OkwrMV/9sMZCJ+Q8Wk8L1hTu4fmR/7Pq5np2a1vJqLcmmYW362HGLUm2FF/oEzdeiP+QNhub0BZrfoR/3p9J/YQy/7L+MPLzcxaNKd7v3T/dWU/C3hvku+rQOevqk92Viv+iPb6o74eH/M5Na4V4w=="
    },
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 3",
      x = 0,
      y = 0,
      width = 40,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYBgFQxVUMjEwVDGRpzeeTH2kgJlAO2bRwR5ywTyg2+bTyH3khC+6nn1A/v4RGn74ALFhO9TCz2GQuXWohd9gA6SG326g2j1AvJdKfqJ2GX8baN4dIL6Lw1xq2qfAhIpHASqIY0KlBxMYzG4baQAA/t8Wmw=="
    }
  }
}
