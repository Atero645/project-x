return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.18.0",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 40,
  height = 19,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "wall",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/wall.png",
      imagewidth = 32,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 2,
      tiles = {}
    },
    {
      name = "magecity",
      firstgid = 3,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/magecity.png",
      imagewidth = 256,
      imageheight = 1450,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 360,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 2",
      x = 0,
      y = 0,
      width = 40,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt0kEKgCAQQNFx3xVypydwF52gdtWqDt9RmhYDBmoRk42g8CFwMY8xAPlnwFoFoLERwt3dU46pzvOdsw1msRnCkS92TymmfJ8pwGc93+7No+8/fRNc9yfdF3tbCT6dKPV/5vKlurN96Xsyu/r4fYuS7Xu7v0a4T/r+qi+fb8U2phxTPZRxDtcLb4w="
    },
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 3",
      x = 0,
      y = 0,
      width = 40,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYEAFjDjwYAFMODAMiKPhwQbk0fAoGAWDEVAzzwtT0SxaAGkqm0co7AZDeYqvbB8M7hssIHSIBsZgSIO1QFxHB3vIBa1A3DbQjsADeoG4b6AdgQdMBeJpA+2IUYAXAADJ9AXE"
    },
    {
      type = "tilelayer",
      name = "Walls",
      x = 0,
      y = 0,
      width = 40,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["collidable"] = "true"
      },
      encoding = "base64",
      compression = "zlib",
      data = "eJzt07FOwkAcx/E7BgUm3YCp+DaMvIcjicACsqgsCAtqGHkQdEIndEEmYTK+ADipX1OaHOflCMFrS8Iv+ST/XpvrL5dWCiGkYoB7PGizjFBCMcQjnrQ5EYGgn5oRnvGizbacoYwKqmue3SSmfmO8YqLNttRQxzkajvu9oYeZNp9a9rnAJa7QdNzvHX0UpO93/kDJsk8L12ij47jfdKkofcG1LV3c4BZ3jvslkfrHd2wTvV8aRzhezqZkXJdSYjq/IFnkDOueszZ/Y+uXx0mIXUyx9YtD9v22i9pvjgU+lfumtaj+3y98C7FyoKY1L5RmftR+BwyHSCpdTGthZle+vzj7AWnNNxM="
    }
  }
}
