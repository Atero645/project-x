return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.17.0",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 80,
  height = 19,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "border",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/border.bmp",
      imagewidth = 32,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "floor",
      firstgid = 2,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../graphics/floor.png",
      imagewidth = 32,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 1",
      x = 0,
      y = 0,
      width = 80,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["collidable"] = "true"
      },
      encoding = "base64",
      compression = "zlib",
      data = "eJztmEEOwCAIBPH/n+4TNE5ogQ7JXndlxERdEbEUUmZ1999ld+9PfrX9d9nd+5Pfd/50bfLj2ZX7O/W/vVPIb/78Va4O/Crvj/z4+SXvsan83pp5+fHs6fwy/0z+wC87W34sW34sWzE9ly0A/w=="
    },
    {
      type = "tilelayer",
      name = "Warstwa Kafelków 2",
      x = 0,
      y = 0,
      width = 80,
      height = 19,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt0WEKACAIg1Hp/ofuBJIkI0ffwL+znhGkmyUc9/7Tbvf/4Te7Hz/8VP3dt+GHXxb89P0OfpPHwW/yffDr9Wf53U+9Ez/8qn5Z8Kv5qXfjh99rP3KfDTzaCeU="
    }
  }
}
