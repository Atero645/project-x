require 'utils'
local scene=require "scene"
local cam=require "camera"
 interface=require "interface"
local cscreen=require 'libs.cscreen'
require "math"

function love.load()
  x = 0
  y = 0
  width=800
  height=600
  scale = 1
  toogle=false
  love.window.setMode(width,height, {vsync=false})
  love.graphics.setNewFont("Oxygen-Regular.ttf",14)
  love.window.setTitle("Project X")
  cscreen.init(width,height,true)
  cam.init(width,height)
  love.mouse.setVisible(false)
  interface.init()
  scene.load()
  love.keyboard.setTextInput( toogle )
end

function love.textinput(t)
  interface.user_input.set(t)
end

function love.draw()
cscreen.apply()
  cam.draw()
  draw_cursor()
  interface.draw()
  cscreen.cease()
end

function love.update(dt)
  scene.update(dt)
  cam.update()
end

function love.keypressed(key)
  interface.user_input.key_callback(key)
  if not toogle then player_1:key_callback(key) end
  if key=="lctrl" then
    toogle= not toogle 
    love.keyboard.setTextInput( toogle )
  end
  if key=="escape" then  love.event.quit() end
 
  if key=="b" then box_on=not box_on else box_on=false end
  if key=="h" then hint=not hint else hint=false end
  if key=="p" then interface.ed_on=not interface.ed_on else interface.ed_on=false end
  if key=="i" then  interface.inv_on=not interface.inv_on else interface.inv_on=false end
end


function love.resize(width, height)
 cscreen.update(width,height)
end

function love.mousepressed( mx, my, button, istouch )
  if button==1 then
    mx,my=cam.toWorld(mx,my)
    scene.addObject(math.ceil((mx-32)/32)*32,math.ceil((my-32)/32)*32,interface.elements_tab[interface.selected])
  end
end

function love.wheelmoved(x,y)
  interface.editor_detect_mouse(y)
end

function draw_cursor()
  love.graphics.setColor(0,0,255,60)
  love.graphics.rectangle("fill",love.mouse.getX()-16,love.mouse.getY()-16,32,32)
end