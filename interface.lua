local interface={
  user_input=require "text_input",
  selected=1,
  ed_on=false,
  inv_on=false,
  message_show=false,
  message_text="",
  elements_tab={},
}

function interface.init()
  interface.user_input.init()
  for i,v in pairs(class_tab) do
    table.insert(interface.elements_tab,i)
  end
end

function interface.statusDraw()
local  mx, my = love.mouse.getPosition( )
 love.graphics.setColor(255,255,255)
  love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )) .. " | Player: X: " .. tostring(player_1.x) .. " Y: " .. tostring(player_1.y).." | mx: "..mx.." my: "..my, 10, 5)
  love.graphics.print("Press h to get hint",500,5)
  love.graphics.print("Coins: "..player_1.gold,20,40)
 end
function interface.draw(w,h)
  
  interface.statusDraw()
  if toogle then interface.user_input.draw(10,580) end
  
  if hint then interface.hint_draw() end
  
  interface.editor_draw()
  interface.inventory_draw()
  
end

function interface.hint_draw()
  love.graphics.setColor(255,255,255)
  love.graphics.print("Press \"L-CTRL\" to enable/disable user input\nUse: WSAD to move player\nPress \"R-CTRL\" to enable/disable lights\nPress ESC to quit",500,32)
  love.graphics.setColor(0,0,255,50)
  love.graphics.rectangle( "fill",500,32,800,500)
  love.graphics.setColor(0,0,255)
  love.graphics.rectangle( "line",500,32,800,500)
end

function interface.editor_draw()
  if interface.ed_on then
    love.graphics.setColor(0,0,0)
    love.graphics.rectangle( "fill",750,0,800,500)

    for i,v in pairs(interface.elements_tab) do
      if i==interface.selected then love.graphics.setColor(255,0,0) else love.graphics.setColor(255,255,255) end
      love.graphics.print(v,750,32*i*0.5)
    end
  end
end

function interface.inventory_draw()
  if interface.inv_on then
    love.graphics.setColor(0,0,0)
    love.graphics.rectangle( "fill",750,0,800,500)
    love.graphics.setColor(255,255,255)
    for i,v in pairs(player_1.inventory) do
      love.graphics.print(v.name,750,32*i*0.5)
    end
  end

end
function interface.messageDraw()
love.graphics.setColor(255,255,255)
love.graphics.rectangle("fill",player_1.x-100,player_1.y-80,280,70)
love.graphics.setColor(0,0,0)
love.graphics.print(interface.message_text,player_1.x-95,player_1.y-50)
end
function interface.editor_detect_mouse(y)

  if y<0 then  
    interface.selected=interface.selected+1 
  elseif y>0 then  interface.selected=interface.selected-1 
  end
  if interface.selected>#interface.elements_tab then 
    interface.selected=1 
  elseif interface.selected<1 then 
    interface.selected=#interface.elements_tab 
  end

end

return interface