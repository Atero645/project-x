local sti = require "libs.sti"
local bump = require "libs.bump"
require "player"
require "coin"
require "box"
require "pillar"
require "plate_box"
require "plate_pillar"
scene={}
objects={}
objects_layer2={}
class_tab={}
class_tab["coin"]=coin
class_tab["plate_box"]=plate_box
class_tab["plate_pillar"]=plate_pillar
class_tab["box"]=box
class_tab["pillar"]=pillar
objects_number=5
colors = { 

    red={255,0,0},
    blue={0,0,255},
    green={0,255,0},
    white={255,255,255},
    black={40,40,40}
}
colors_index={"red","blue","green","white","black"}
object_index={"box","pillar"}
grid = {}

function scene.generate(ilosc)

  for i = 1, 16 do
    grid[i] = {}

    for j = 1, 18 do
      grid[i][j] ="0"  -- Fill the values here
    end

  end
  for i=1,ilosc do
    grid[love.math.random(1,16)][love.math.random(2,17)] = {object_index[love.math.random(1,2)],colors_index[love.math.random(1,5)]}
  end
end

function scene.applayGrid()
  for i=1,16 do
    for j=1,18 do
      if grid[i][j]~="0" then scene.addObject(i*32+32*20,j*32,grid[i][j][1],grid[i][j][2]) end
    end
  end
  for i,v in pairs(objects) do
   local x=32*love.math.random(1,16)+32*20
   local y=32*love.math.random(2,17)
   if v.typ=="box" then scene.addObject(x,y,"plate_box",v.color) end
   if v.typ=="pillar" then scene.addObject(x,y,"plate_pillar",v.color) end
  end
end

function scene.addObject(x,y,name,c)
  local tmp=class_tab[name](x,y,c)
  if tmp.typ=="plate_box" or tmp.typ=="plate_pillar" then  table.insert(objects_layer2,tmp)else table.insert(objects,tmp) end
  world:add(tmp,tmp.x,tmp.y,tmp.w,tmp.h)
end

function scene.coins()

  for i=32,15*32,32 do
    scene.addObject(i,288+3*32,"coin")
  end
end

function scene.objects_draw()
for i,object in pairs(objects_layer2) do
    if object.hidden==0 then object:draw() end
  end
  for i,object in pairs(objects) do
    if object.hidden==0 then object:draw() end
  end
end

function scene.load(name)
  world=bump.newWorld(32)
  map=sti.new("levels/level4.lua",{"bump"})
  map:bump_init(world)

  player_1=player(320,320)
  world:add(player_1,player_1.x,player_1.y,player_1.w,player_1.h)

  scene.coins()
  rand = love.math.random

  scene.generate(objects_number)
  scene.applayGrid()
end

function scene.update(dt)
  map:update(dt)
  player_1:update(dt)
  for i,coin in pairs(objects) do
    if coin.typ=="coin" then
      coin:update(dt)
      if coin.hidden==1 then table.remove(objects,i) end
    end
  end
  if objects_number==0 then interface.message_text="Wygrałeś" interface.message_show=true end
end

function scene.draw()
  map:draw()
  scene.objects_draw()
  player_1:draw()
end

return scene
