require 'libs/class'
class"plate_pillar"
function plate_pillar:plate_pillar(x,y,c,s)
  self.x=x
  self.y=y
  self.w=32
  self.h=32
  self.hidden=0
  self.typ="plate_pillar"
  self.name="plate_pillar"
  if c then self.color=c else self.color="white" end
  self.size=s
  self.darkcolor={}

  self.image=love.graphics.newImage('graphics/plate_pillar.png')
  if self.color=="white" then self.quad = love.graphics.newQuad(0, 0, self.w, self.h, self.image:getDimensions()) end
  if self.color=="blue" then self.quad = love.graphics.newQuad(0, 32, self.w, self.h, self.image:getDimensions()) end
  if self.color=="green" then self.quad = love.graphics.newQuad(0, 64, self.w, self.h, self.image:getDimensions()) end
  if self.color=="red" then self.quad = love.graphics.newQuad(0, 96, self.w, self.h, self.image:getDimensions()) end
  if self.color=="yellow" then self.quad = love.graphics.newQuad(0, 128, self.w, self.h, self.image:getDimensions()) end
  if self.color=="black" then self.quad = love.graphics.newQuad(0, 160, self.w, self.h, self.image:getDimensions()) end
  
end

function plate_pillar:draw()

 
    love.graphics.draw(self.image, self.quad, self.x, self.y)

end

function plate_pillar:update(dt)
  local selfFilter = function(item, other)
    if     other.typ=="box"   then return 'cross'
    else return 'slide'
    end
    
  end    
    self.x, self.y, cols, len = world:move( self, self.x, self.y,selfFilter )

    
    for i,col in pairs(cols) do
      print('collided with ' .. tostring(col.other.typ))
      print(col.normal.x.." "..col.normal.y)

      if col.other.typ=="box" or col.other.typ=="pillar" then
        print('collided with ' .. tostring(col.other.typ))
      end
  end
end

