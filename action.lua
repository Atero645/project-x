local action={
  last_command=nil,
  data={},
  interrupt=false
}

local function move_player(t)
  if t[2]=="up" then player_1.ydest=player_1.ydest-t[3]*32
  elseif t[2]=="down" then player_1.ydest=player_1.ydest+t[3]*32
  elseif t[2]=="left" then player_1.xdest=player_1.xdest-t[3]*32
  elseif t[2]=="right" then player_1.xdest=player_1.xdest+t[3]*32
  end
end

local function grab_object(t)

  if #t~=3 then
    action.interrupt=true
    interface.message_text="Podaj kolor klocka który mam podnieść"
    interface.message_show=true
    coroutine.yield()
    t={t[1],action.data[1][1],t[2]}
    
  end
  local count={};
  for i,object in pairs(objects) do

    if object.typ==t[3]  and object.color==t[2] and object.fixed==false then --to do check object properties from user input instead of checking fixed color now
      table.insert(count,i)
      --player_1:grab(object)
    end

  end
  if #count>1 then
    action.interrupt=true
    interface.message_text="Znalazłem "..#count.." obiektów danego koloru\nPodaj numer obiektu który mam podnieść"
    interface.message_show=true
    for i,v in pairs(count) do objects[v].num=i objects[v].show_num=true end
    coroutine.yield()
    for i,v in pairs(count) do  objects[v].show_num=false end
    
    player_1:grab(objects[count[action.data[1][1]]])
  else
   player_1:grab(objects[count[1]])
   end
  
end

function action.setAction(c)

  if c~=nil and #c then
local status=0;
    for i,v in pairs(c) do

      if v[1]=="move_player" then 
            move_player(v) 
            player_1.ready=false
            status=1
            
      elseif v[1]=="take"  then
      if player_1.hands~=nil then
      
      action.interrupt=true
    interface.message_text="Mam zajęte ręce czy mam odlozyć obiekt ?"
    interface.message_show=true
    coroutine.yield()
    if(action.data[1][1]=="yes") then player_1:drop() end
    end
      
      
             grab_object(v)
             player_1.ready=false
             status=1
             
      elseif v[1]=="put" then 
             player_1:drop() 
             player_1.ready=false
             status=1
      elseif v[1]=="rotate" then
            if v[2]=="left" then if player_1.stani >1 then player_1.stani=player_1.stani-1 else player_1.stani=4 end
            elseif v[2]=="right" then if player_1.stani <4 then player_1.stani=player_1.stani+1 else player_1.stani=1 end
            end
            print(player_1.stani)
            status=1
      elseif v[1]=="move_object" then
          print("step1")
          local tmp=v
          table.remove(c,i)
          table.insert(c,i,{"take",tmp[2],tmp[3]})
          table.insert(c,i+1,{"move_player",tmp[5],tmp[4]})
          --table.insert(c,i+2,{"wait"})
          --table.insert(c,i+3,{"put"})
          --action.data="put"
          status=1
          for l,k in pairs(c) do print(k) end
          action.setAction(c)
          break
        
      
      end
      
      if player_1.ready == false then coroutine.yield() end
      
      if #v==0 then table.remove(c,i) end --mozna wykrywac ilosc nieznanych slow pusta tabela==nieznany ciag znakow
      
      if status==0 then interface.message_text="Niestety ale nie rozumiem" interface.message_show=true else interface.message_show=false end
    end
  end
  

  action.last_command=c
  action.print()
end

function action.print()

  if action.last_command then
    for i,v in pairs(action.last_command) do
      print(v)
    end
  end
end

return action
