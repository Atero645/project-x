local utf8 = require("utf8")
local utf_8=require 'libs.utf8'
local text_input={}
local text=""
result={}
text_module=require'text_module'
action=require'action'
co=nil

function text_input.init()
  text_module.load()
  love.keyboard.setKeyRepeat(true)
  text=""
end

function text_input.set(t)
  text=text .. t
end

function text_input.draw(x,y)

  love.graphics.setColor(128,128,128,192)
  love.graphics.rectangle( "fill",0,height-30,width,30)
  love.graphics.setColor(255,255,255)
  love.graphics.printf("Enter command: "..text, 10, height-20, love.graphics.getWidth())
  
end

function text_input.key_callback(key)
  if key == "backspace" then
    -- get the byte offset to the last UTF-8 character in the string.
    local byteoffset = utf8.offset(text, -1)

    if byteoffset  then
      -- remove the last UTF-8 character.
      -- string.sub operates on bytes rather than UTF-8 characters, so we couldn't do string.sub(text, 1, -2).
      text = string.sub(text, 1, byteoffset - 1)
    end
  elseif key=="return" then
    process_command(text) -- Process given command by text module.
    text=""
  end
end

function process_command(command)

  command=command:lower()
  command=utf_8.gsub(command,"ó","o")
  command=utf_8.gsub(command,"ą","a")
  command=utf_8.gsub(command,"ż","z")
  command=utf_8.gsub(command,"ź","z")
  command=utf_8.gsub(command,"ę","e")
  command=utf_8.gsub(command,"ś","s")
  command=utf_8.gsub(command,"ć","c")
  command=utf_8.gsub(command,"ł","l")
  res=text_module.get_text(command)
  if action.interrupt then
      action.data=res
      action.interrupt=false
      interface.message_show=false
      coroutine.resume(co)
  else
  co=coroutine.create(function() action.setAction(res) end)
  print(coroutine.status(co)) 
  coroutine.resume(co)
end
end

return text_input