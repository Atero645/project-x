require 'libs/class'
class"pillar"
function pillar:pillar(x,y,c,s)
  self.x=x
  self.y=y
  self.w=32
  self.h=32
  self.r=16
  self.hidden=0
  self.typ="pillar"
  self.name="pillar"
  self.fixed=false
  if c then self.color=c else self.color="white" end
  self.size=s
  self.num=0
  self.show_num=false
  --self.darkcolor={}
  --self.darkcolor[1]=colors[self.color][1]-80
  --self.darkcolor[2]=colors[self.color][2]-80
  --self.darkcolor[3]=colors[self.color][3]-80
  self.grabbed=false
  self.image=love.graphics.newImage('graphics/pillar.png')
  if self.color=="white" then self.quad = love.graphics.newQuad(0, 0, self.w, self.h, self.image:getDimensions()) end
  if self.color=="blue" then self.quad = love.graphics.newQuad(0, 32, self.w, self.h, self.image:getDimensions()) end
  if self.color=="green" then self.quad = love.graphics.newQuad(0, 64, self.w, self.h, self.image:getDimensions()) end
  if self.color=="red" then self.quad = love.graphics.newQuad(0, 96, self.w, self.h, self.image:getDimensions()) end
  if self.color=="yellow" then self.quad = love.graphics.newQuad(0, 128, self.w, self.h, self.image:getDimensions()) end
  if self.color=="black" then self.quad = love.graphics.newQuad(0, 160, self.w, self.h, self.image:getDimensions()) end
  
end

function pillar:draw()
  --love.graphics.setColor(colors[self.color])

  --love.graphics.pillar( "fill",self.x+16,self.y+16,self.r,100)

  --love.graphics.setColor(self.darkcolor)
  --love.graphics.pillar( "fill",self.x+16,self.y+16,self.r-6,100)
    love.graphics.draw(self.image, self.quad, self.x, self.y)
    if self.show_num then 
  love.graphics.setColor(0,0,0)
  love.graphics.print(self.num,self.x+8,self.y+8)
  love.graphics.setColor(255,255,255)
  end
end
function pillar:remove_col() --remove collisions when item is in player's hands
world:remove(self)
end
function pillar:move(x,y)

 local selfFilter = function(item, other)
    if     other.typ=="coin"   then return 'cross'
    elseif other.typ=="plate_pillar" then return 'cross'
    else return 'slide'
    end
  end
  if self.fixed==false then

  self.x,self.y,cols,len=world:move(self,x,y,selfFilter)
  end
  for i,col in pairs(cols) do
    if col.other.typ=="plate_pillar" and col.other.color==self.color and self.fixed==false then self.fixed=true objects_number=objects_number-1 end
    if col.other.typ~="player" or self.fixed==true then player_1.xdest=player_1.x player_1.ydest=player_1.y end --reset players movement queue
  end
  
end
