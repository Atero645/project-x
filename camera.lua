local gamera=require "libs.gamera"
local camera={}

function camera.init(w,h)
  cam=gamera.new(0,0,2000,600)
  --cam:setWindow(0,0,800,600)
end

function camera.update()
  cam:setPosition(player_1.x,player_1.y)
end

function camera.draw()
  cam:draw(function(l,t,w,h)
      scene.draw()
      if interface.message_show then interface.messageDraw() end
    end)
end

function camera.toWorld(x,y)
  x,y=cam:toWorld(x,y)
  return x,y
end

return camera