local lpeg=require'lpeg'
local base=require'base_words'
local text_module={}
local words_base={}
P,R,C,Cs,Ct,V=lpeg.P,lpeg.R,lpeg.C,lpeg.Cs,lpeg.Ct,lpeg.V

function text_module.load()
  base.initBase()
  base.buildBase()
  words_base=base.getBase()
  move=words_base['go_words']
  directions=words_base['directions']
  digit=words_base['digit'] 
  pola=white*(P"pola"+P("pole")+P"pol"+P"kroki"+P"krok")^0
  use=words_base['use_words']
  object=words_base["objects"]
  take=words_base["take_words"]
  put=words_base["put_words"]
  adjective=words_base["adjectives"]
  rotate=words_base["rotate"]
  move_obj=words_base["move_words"]
  dec=Cs(P"tak"/"yes")
end

function node_1(p)
  return p / function(move, digit, direction)
    return  move, direction, digit 
  end
end

function anywhere (p) --funcja szukająca frazy w stringu z dokumentacji lpega
  return lpeg.P{ p + 1 * lpeg.V(1) }
end

function text_module.get_text(text)
 
  fraza=move*directions*digit + 
  node_1(move*digit*directions) + 
  node_1(move*digit*white*"w"*directions) + 
  move*white*"w"*directions*digit*pola + 
  node_1(move*digit*pola*white*"w"*directions)
                                             
  fraza2=use*adjective*object + use*object
  fraza3=take*adjective*object+take*object + adjective
  fraza4=put*object
  fraza5=rotate*white*"w"*directions
  fraza6=move_obj*adjective*object*digit*pola*white*"w"*directions
  fraza=fraza6+fraza+fraza2+fraza3+fraza4+fraza5+digit+dec+1
  fraza=Ct(fraza)
  fraza=fraza^1
 
  zdanie=Ct(anywhere(fraza))
  result=zdanie:match(text) 
  return result
end

return text_module
