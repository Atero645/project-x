local utf8 = require("utf8")
local lpeg = require'lpeg'
P,R,C,Cs,Ct=lpeg.P,lpeg.R,lpeg.C,lpeg.Cs,lpeg.Ct

white = lpeg.S(" \t\r\n") ^ 0
local base_words = {}
local phrasesdir = "phrases"
files = {}
base = {}

function string:split(delimiter) --Not by me
  local result = {}
  local from  = 1
  local delim_from, delim_to = string.find( self, delimiter, from  )
  while delim_from do
    table.insert( result, string.sub( self, from , delim_from-1 ) )
    from = delim_to + 1
    delim_from, delim_to = string.find( self, delimiter, from  )
  end
  table.insert( result, string.sub( self, from  ) )
  return result
end


function base_words.initBase()
  --get files list
  if love.filesystem.exists(phrasesdir) then
    local l = love.filesystem.getDirectoryItems(phrasesdir)
    for i,file in ipairs(l) do
      --if file:match(".lst")  then
      files[i] = file
      --end
    end
  end
end

function base_words.addToBase(file)
  if love.filesystem.exists(phrasesdir .. "/" .. file) then
    local s = love.filesystem.read(phrasesdir .. "/" .. file)
    local s1 = s:split("\n")
    local key = ""
    local m1 = ""
    local m2 = ""
    local words = nil
    local word = nil
    for j = 1, #s1 do
      if s1[j]:sub(1,1) == "=" then
        key = s1[j]:sub(2)
        --print("\nkey " .. key)
      elseif s1[j]:sub(1,1) == "~" then
        m1 = s1[j]:sub(2)
        --print("\nm1 " .. m1)
      else
        local s2 = s1[j]:split("~")

        if s2[2] ~= '' and s2[2] then
          m2 = s2[2]
          word = P(s2[1])
          --print(s2[1] .. " " .. m2)
          if words==nil then
            words=Cs(white* word / m2)
          else
            words = words + Cs(white * word / m2) 
          end
        elseif s2[2] == nil then
          --print("\n"..s2[1])
          if words==nil then 
            words=P(s2[1]) 
          else 
            words = words + P(s2[1]) 
          end
        end
      end
    end
    if m1 ~= '' and m1 then 
      base[key]=Cs(white * words / m1)
    else
      base[key]=words
    end
  end
end

function base_words.buildBase()
  for i,file in ipairs(files) do
    base_words.addToBase(file)
  end

  --  up=Cs(P'gora'+P'gore' / "up")
  --  down=Cs(P'dol' / "down")
  --  left=Cs(P'lewo' / "left")
  --  right=Cs(P'prawo' / "right") --definicja kierunków z podstawieniem
  --  base["directions"]=white*(up+left+down+right) --złożenie wszystkich możliwoci za pomocą klauzuli "lub"
  base["digit"]=white*R'09'^1 / tonumber --definicja liczb
end


function base_words.getBase()
  return base
end

return base_words
