#!/bin/sh

#Change to project X directory
CANONPATH=`readlink -f "$0"`
cd="`dirname "$CANONPATH"`"

MACHINE=`uname -m`
if [ "$MACHINE" = x86_64 ]
then 
	LIBS=./bin/lib64
	BIN=./bin/love.x86_64
else
	LIBS=./bin/lib32
	BIN=./bin/love.x86
fi


# Run love
export LD_LIBRARY_PATH=$LIBS:"$LD_LIBRARY_PATH"
$BIN . $@
