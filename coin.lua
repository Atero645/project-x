require 'libs/class'
local anim8=require'libs.anim8'
class("coin")

function coin:coin(x,y)
  self.x=x
  self.y=y
  self.w=32
  self.h=32
  self.typ="coin"
  self.hidden=0
  self.image=love.graphics.newImage('graphics/coin.png')
  self.g=anim8.newGrid(16,16,self.image:getDimensions())
  self.animation=anim8.newAnimation(self.g('1-11',1),0.1)
  self.name="coin"
end

function coin:update(dt)
  self.animation:update(dt)
end

function coin:draw()
  love.graphics.setColor(255,255,255)
  self.animation:draw(self.image,self.x+8,self.y+8)
end

function coin:remove()
  world:remove(self)
end

