print_o=print
function print(t) --print table

  if type(t)=="table" then
    text="{"
    for i,v in pairs(t) do
      text=text..(#text>1 and ", " or "")..( type(i)=="string" and "'"..i.."'"..":" or "").. ( type(v)=="table" and "table" or tostring(v) )
    end
    text=text.."}"
    print_o(text)
  else
    print_o(t)
  end
end
  